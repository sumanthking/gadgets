from django.urls import path
from . import views

urlpatterns = [
    path('products/', views.product_list, name='product_list'),
    path('cart/add/', views.cart_add, name='cart_add'),
    path('cart/', views.cart_view, name='cart_view'),
    path('cart/delete/<int:cart_item_id>/', views.cart_delete, name='cart_delete'),
    path('login/', views.CustomLoginView.as_view(), name='login'),
    path('logout/', views.CustomLogoutView.as_view(), name='logout'),
    path('cart/', views.cart_view, name='cart_view'),
    path('contact/', views.contact_view, name='contact_view'),
    path('help/', views.help_view, name='help_view'),
    path('cart/increment/<int:cart_item_id>/', views.increment_quantity, name='increment_quantity'),
    path('cart/decrement/<int:cart_item_id>/', views.decrement_quantity, name='decrement_quantity'),
    path('cart/delete/<int:cart_item_id>/', views.delete_from_cart, name='delete_from_cart'),
    
    
]

    
